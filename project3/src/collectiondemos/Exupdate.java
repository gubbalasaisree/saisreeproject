package collectiondemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.javatraining.dbcon.DBConfig;

public class Exupdate {
	public static void main(String[] args) throws SQLException {
		Scanner sc=new Scanner(System.in);

		Connection conn=DBConfig.getConnection();
		
		System.out.println("Enter customer ID");
		int cId = sc.nextInt();
		
		System.out.println("Enter customer Address");
		String cAdd =sc.next();
		
		System.out.println("Enter bill amount");
		int bAmt = sc.nextInt();
		
		
		String query = "update customer set customer_add = ? , bill_amnt=? where customer_id = ?";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString(1,cAdd );
	      preparedStmt.setInt(2, bAmt);
	      preparedStmt.setInt(3, cId);

	      
	      int rows = preparedStmt.executeUpdate();
	      System.out.println(rows + " Updated");
	      
	      conn.close();
	}

}

