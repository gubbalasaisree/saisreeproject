package collectiondemos;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Demo1 {
	public static void main(String[] args) throws ClassNotFoundException, SQLException
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		//System.out.println("driver loaded");
		Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:orcl","system","system");
		//System.out.println("connected");
		Statement statement=connection.createStatement();
		ResultSet res=statement.executeQuery("select * from customer");
		while(res.next()) {
			System.out.println(res.getString(1));
			System.out.println(res.getString(2));
			System.out.println(res.getString(3));
			System.out.println(res.getString(4));
		}
		res.close();
		statement.close();
		connection.close();
		
		
		}

	}