package excepdemo;

public class Demo4 {
	int i=10;
	public int display() 
	{
		try
		{
			i++;
			if(i==11)
				return i;		
		}
		catch (Exception e)
		{
			i++;
		}
		finally
		{
			
			System.out.println("finally called");
			i++;
		}
		
		i++;
		System.out.println("thanks called" );
		return i;
	}
	public static void main(String[] args)
	{
		Demo4 d=new Demo4();
		System.out.println(d.display());
		
	}

}
