package excepdemo;

public class Demo2 {
	public static void main(String[] args)
	{
		String marks="98";
		System.out.println(marks+10);
		//object-->primitive
		int m=Integer.parseInt(marks);
		System.out.println(m+10);
		
		//primitive to object(boxing)
		int num=100;
		Integer num1=num; //auto boxing
		System.out.println(num1);
		
		//object-->primitive
		Integer scores=100;
		int lscores=scores;
		System.out.println(lscores);     
		
		
		
	}

}
