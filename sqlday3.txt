select mod(9,2),sqrt(9) from dual;
 MOD(9,2)    SQRT(9)
---------- ----------
         1          3
-------------------------------------------------
select power(3,3) from dual;
POWER(3,3)
----------
        27
-------------------------------------------------
select to_char(dob,'dd_mon/ww'),f_name,dob from s_details;
TO_CHAR(D F_NAME               DOB
--------- -------------------- ---------
01_jan/01 rohit                01-JAN-94
27_nov/48 rahul                27-NOV-97
02_sep/35 noel                 02-SEP-98
24_oct/43 kp                   24-OCT-94
10_aug/32 paresh               10-AUG-93
24_apr/17 barjindar            24-APR-90
24_feb/08 shreya               24-FEB-91
14_sep/37 mohit                14-SEP-94
22_dec/51 priyanka             22-DEC-96
13_feb/07 samuel               13-FEB-93

10 rows selected.
------------------------------------------------------------------
select to_char(dob,'month'),count(*) from s_details group by (dob,'month');
TO_CHAR(D   COUNT(*)
--------- ----------
april              1
december           1
august             1
september          1
october            1
february           1
november           1
february           1
january            1
september          1

10 rows selected.
----------------------------------------------------------------------------------
DDL COMMANDS

create table temp1 as select * from s_details;
Table created.

 desc temp1;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 STUD_ID                                            NUMBER(4)
 F_NAME                                             VARCHAR2(20)
 L_NAME                                             VARCHAR2(20)
 HOUSE_NO                                           NUMBER(20)
 ADDR_1                                             VARCHAR2(25)
 ADDR_2                                             VARCHAR2(25)
 DOB                                                DATE
 ROLLNUM                                            NUMBER(10)

-------------------------------------------------------------------------------------------
 create table temp2 as select *from s_details where stud_id is null;

Table created.

SQL> desc temp2;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 STUD_ID                                            NUMBER(4)
 F_NAME                                             VARCHAR2(20)
 L_NAME                                             VARCHAR2(20)
 HOUSE_NO                                           NUMBER(20)
 ADDR_1                                             VARCHAR2(25)
 ADDR_2                                             VARCHAR2(25)
 DOB                                                DATE
 ROLLNUM
 select * from temp2;

no rows selected
------------------------------------------------------------------------
alter table temp1 drop column rollnum;
Table altered.

SQL> desc temp1;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 STUD_ID                                            NUMBER(4)
 F_NAME                                             VARCHAR2(20)
 L_NAME                                             VARCHAR2(20)
 HOUSE_NO                                           NUMBER(20)
 ADDR_1                                             VARCHAR2(25)
 ADDR_2                                             VARCHAR2(25)
 DOB                                                DATE
----------------------------------------------------------------------------------------------
alter table temp1 add rollnum number(5);
Table altered.

SQL> desc temp1;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 STUD_ID                                            NUMBER(4)
 F_NAME                                             VARCHAR2(20)
 L_NAME                                             VARCHAR2(20)
 HOUSE_NO                                           NUMBER(20)
 ADDR_1                                             VARCHAR2(25)
 ADDR_2                                             VARCHAR2(25)
 DOB                                                DATE
 ROLLNUM
------------------------------------------------------------------------
alter table temp1 modify stud_id number(10);

Table altered.

SQL> alter table temp1 rename column stud_id to s_id;

Table altered.
---------------------------------------------------------
 alter table temp2 rename to temp3;

Table altered.
--------------------------------------------------------
drop table temp3;

table dropped;
---------------------------------
Flashback query-
------------------


 select * from temp1 as of timestamp(to_date('29/08/2018 10:20','dd/mm/yyyy hh24:mi'));


 insert into temp1(select * from temp1 as of timestamp(to_date('29/08/2018 10:20','dd/mm/yyyy hh24:mi')));
Truncate: Removes the structure


	truncate table temp1;
table truncated.



 insert into s_details values(1005, 'sully', 'sameer', null, null, null, '25-feb-2000', 47);
1 row created.
 insert into s_details(f_name,stud_id)values('mahir',1006);
1 row created.


-------------------


DCL:
------


create user *username* identified by *password*

Grant:
------

SQL> grant create session to noel;

Grant succeeded.

SQL> grant select on temp1 to noel;

Grant succeeded.

SQL> grant select on temp1 to noel;

Grant succeeded.


SQL> grant insert on temp1 to noel;

Grant succeeded.

SQL> grant create any table to noel;

Grant succeeded.





Revoke:

SQL> revoke create any table from noel;

Revoke succeeded.create or replace procedure ramzan is 
a1 date;
begin
a1:=to_date('16-05-2018','dd-mm-yyyy');
for i in 1..10 loop
 a1:=a1+(29.53*12);
 dbms_output.put_line(a1||to_char(a1,'month day'));
end loop;
end ramzan;
/



SQL> execute ramzan;
05-MAY-19may       sunday
23-APR-20april     thursday
13-APR-21april     tuesday
02-APR-22april     saturday
22-MAR-23march     wednesday
11-MAR-24march     monday
28-FEB-25february  friday
17-FEB-26february  tuesday
07-FEB-27february  sunday
27-JAN-28january   thursday

SQL> revoke select on temp1 from noel;

Revoke succeeded.


-------------------------------------------------










