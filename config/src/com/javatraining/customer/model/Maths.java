package com.javatraining.customer.model;

public class Maths {
	private int marks;
	private String grade;
	
	public Maths()
	{
		
	}

	public Maths(int marks, String grade) {
		super();
		this.marks = marks;
		this.grade = grade;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "Maths [marks=" + marks + ", grade=" + grade + "]";
	}
	
	
	
	

}
