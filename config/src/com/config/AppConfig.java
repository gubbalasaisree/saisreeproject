package com.config;

import javax.management.MXBean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;

public class AppConfig {
	
@Bean
@Scope("prototype")
public Customer getCustomerObject()
	{
	  return new Customer();	
	}
	
	@Bean
	public BankAccount getBankAccount()
	{
		return new BankAccount();
	}

}
