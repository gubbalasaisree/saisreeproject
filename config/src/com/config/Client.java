package com.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;

public class Client {
	
	public static void main(String[] args) {
		
	
	
	ApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
	
	
	Customer c=context.getBean(Customer.class);
	c.setCustomerName("mohan");
	c.setCustomerId(100);
	c.setCustomerAddress("mumbai");
    c.setBillAmnt(20000);
    
    Customer c1=context.getBean(Customer.class);
	c.setCustomerName("mohit");
	c.setCustomerId(100);
	c.setCustomerAddress("mumbai");
    c.setBillAmnt(20000);
    
    
    BankAccount b1=context.getBean(BankAccount.class);
    b1.setAccountnumber(123456);
    b1.setBalance(25000);
    
    BankAccount b2=context.getBean(BankAccount.class);
    b1.setAccountnumber(234567);
    b1.setBalance(25000);
    
    
    //c.setBankaccount(b1);
     System.out.println(c);
     System.out.println(c1);
	
	}
	

}
