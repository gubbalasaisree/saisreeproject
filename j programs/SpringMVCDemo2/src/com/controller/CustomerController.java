package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;
import com.javatraining.customer.service.CustomerService;

@Controller
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	@Autowired
	CustomerDAO customerDAO;
	@RequestMapping("/CustomerDetails")
	public ModelAndView getCustomerView(Customer c)
	{
		
		
		customerDAO.insertCustomerDetails(c);
		ModelAndView view=new ModelAndView();
		
		
		view.setViewName("CustomerDetails");
		view.addObject("x",c.getCustomerName()+"you have successfully entered the data");
		
		return view;
	}
	
	

}
