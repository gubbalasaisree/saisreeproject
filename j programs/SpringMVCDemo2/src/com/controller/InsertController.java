package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class InsertController {
	
@RequestMapping("/insert")
public ModelAndView getA()
{
	ModelAndView view=new ModelAndView();
	view.setViewName("insert");
	view.addObject("message", "Hello Good Morning");
	return view;
}


@RequestMapping("/Guest")
public ModelAndView getGuestView(Guest guest)
{
	ModelAndView view=new ModelAndView();
	view.setViewName("guestdetails");
	view.addObject("message", "Hello Good Morning");
	view.addObject("guestInfo", guest);
	return view;
}

}

