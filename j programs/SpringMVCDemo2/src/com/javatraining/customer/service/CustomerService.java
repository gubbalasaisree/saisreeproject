package com.javatraining.customer.service;

import java.util.List;

import com.controller.CustomerDetails;
import com.javatraining.customer.model.Customer;


public interface CustomerService {
	
	public int insertCustomerDetails(Customer customer);
	public int UpdateCustomerDetails(int customerId,String newCustomerAddress,int newBillAmount);
	public int deleteCustomer(int customerId);
	public Customer findByCustomer_id(int customerId);
	public boolean isCustomerAExists(int customerId);
	public List<Customer> listAllCustomers();
	
	
}