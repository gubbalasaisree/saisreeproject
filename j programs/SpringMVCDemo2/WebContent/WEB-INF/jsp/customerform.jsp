<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<html>

<body>
Insert Customer Details Using Spring Forms:
<f:form action="customerF.do" commandName="command">
Customer Id:<f:input path="customerId"/>
Customer Name:<f:input path="customerName"/>
Customer Address:<f:input path="customerAddress"/>
Bill Amount:<f:input path="billAmount"/>
<input type="submit" value="go">
</f:form>
</body>
</html>