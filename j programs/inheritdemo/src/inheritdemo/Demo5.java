package inheritdemo;
import java.util.*;
import Collectiondemo.AddressComparator;
public class Demo5 {
	public static void main(String[] args) {
		Customer c1=new Customer(1,"mohan","pune",9800);
		Customer c2=new Customer(2,"anu","mumbai",1800);
		Customer c3=new Customer(3,"sai","chennai",9000);
		Customer c4=new Customer(4,"noel","hyderabad",800);
		Customer c5=new Customer(5,"harika","jaipur",980);


		
		List<Customer> allc=new ArrayList<Customer>();
		allc.add(c1);
		allc.add(c2);
		allc.add(c3);
		allc.add(c4);
		allc.add(c5);
		
		System.out.println(allc);
		
		 Collections.sort(allc,new AddressComparator());
		
	    System.out.println("after sorting");
	   
	    System.out.println(allc); 
	    Collections.sort(allc,new Comparator<Customer>()
	    		{
	    	@Override
	    	public int compare(Customer o1, Customer o2) {
	    		if(o1.getCustomerId()<o2.getCustomerId())
	    			return 1;
	    		else
	    			return -1;
	    	
	    	}
	    		});
	    
		}
	}


