package inheritdemo;

abstract class Bike extends Vehicle
{
	
	public void start() {
	 
		System.out.println("\nBike started");
	}
	public abstract void kickstart();
}



class Car extends Vehicle {
	String carType;
	String color="White";
	public void start()
	{
		System.out.println("car started");
	}
	
	public void showDetails()
	{noOfWheels=4;
	carType="HatchBack";
	
	System.out.println("Car color is:" +color);
	System.out.println("Car color is:" +super.color);
	System.out.println("Car has noOfWheels:" +noOfWheels);
	System.out.println("CarType is:" +carType);
}

@Override
public void stop() {
System.out.println("car stopped");
}

}




class Pulsar extends Bike
{
public void kickstart() {
System.out.println("bike kickstarted");

}

@Override
public void stop() {
	System.out.println("bike stopped");
}

}






abstract class Vehicle {
String color="Blue";
int noOfWheels;
public abstract void start();
public abstract void stop();
}