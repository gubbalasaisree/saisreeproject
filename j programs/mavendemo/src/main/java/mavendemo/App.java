package com.javatraining.maven.mavendemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws ClassNotFoundException, SQLException
    {
    	Customer customer=new Customer(1200,"tufail","mumbai",8500);
    	
    	Configuration configuration=new Configuration().configure();
    	
    	SessionFactory factory=configuration.buildSessionFactory();
    	
    	Session session= factory.openSession();
    	Transaction transaction=session.beginTransaction();
    	
    	session.save(customer);
    	
    	transaction.commit();
    	
    	session.close();
    	
    	factory.close();
    	
    	System.out.println("data stored");
    }
}
