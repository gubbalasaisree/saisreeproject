package com.javatraining.customer.model;

public class BankAccount {
	private int accountnumber;
	private int balance;
	public BankAccount(int accountnumber, int balance) {
		super();
		this.accountnumber = accountnumber;
		this.balance = balance;
	}
	public int getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(int accountnumber) {
		this.accountnumber = accountnumber;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public BankAccount()
	{
		
	}
	@Override
	public String toString() {
		return "BankAccount [accountnumber=" + accountnumber + ", balance=" + balance + "]";
	}

}
