package com.javatraining.customer.model;

public class Student {
	
	private English english;
	private Maths maths;
	private Science science;
	
	
	public Student()
	{
		
	}


	public Student(English english, Maths maths, Science science) {
		super();
		this.english = english;
		this.maths = maths;
		this.science = science;
	}


	public English getEnglish() {
		return english;
	}


	public void setEnglish(English english) {
		this.english = english;
	}


	public Maths getMaths() {
		return maths;
	}


	public void setMaths(Maths maths) {
		this.maths = maths;
	}


	public Science getScience() {
		return science;
	}


	public void setScience(Science science) {
		this.science = science;
	}


	@Override
	public String toString() {
		return "Student [english=" + english + ", maths=" + maths + ", science=" + science + "]";
	}
	
	
	
	
	

}
