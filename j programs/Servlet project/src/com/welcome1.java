package com;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class welcome1
 */
@WebServlet("/welcome1")
public class welcome1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public welcome1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
    int counter=0;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		counter++;
		String username=request.getParameter("uname");
		String upass=request.getParameter("upass");
		boolean visited=false;
	    Cookie c[]=request.getCookies();
	    
	    if(c!=null)
	    {
	    	for(Cookie c1:c)
	    	{
	    		if(c1.getName().equals(username)&&c1.getValue().equals(upass))
	    		{
	    			visited=true;
	    			break;
	    		}
	    	}
	    }
	    if(visited)
	    {
	    	response.getWriter().println("<h1>you already visited-"+username+"</h1>");
	    }
	    else
	    {
	    	response.getWriter().println("you are first time visitor"+username);
	    	Cookie cookie=new Cookie(username,upass);
	    	response.addCookie(cookie);
	    }
	}
}
