package com.javatraining.customer.dao;

import java.util.List;

import com.javatraining.customer.model.Customer;

public interface CustomerDAO {
	public int InsertCustomerDetails(Customer customer);
	public int UpdateCustomerDetails(int customerId,String newCustomerAddress,int newBillAmount);
	public int deleteCustomer(int customerId);
	public Customer findByCustomer_id(int customerId);
	public boolean isCustomerAExists(int customerId);
	public List<Customer> listAllCustomers();
	
	

}
