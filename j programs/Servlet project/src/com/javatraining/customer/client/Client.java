package com.javatraining.customer.client;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {
		Customer customer1=new Customer(999,"sharukh","mumbai",99000);
		
		CustomerDAO customerDAO=new CustomerDAOImpl();
		
		//testing method insert
		
		/*int result=customerDAO.InsertCustomerDetails(customer1);
		System.out.println(result+"rows affected");*/
		
		// testing update method.
		int result1=customerDAO.UpdateCustomerDetails(100,"jammu",8520);
		System.out.println(result1+"rows affected");
		

	}

}
