package com;



import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class customerinfo1
 */
@WebServlet("/customerinfo1")
public class customerinfo1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public customerinfo1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void service(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
    	int id=Integer.parseInt(request.getParameter("customerId"));
    	String name=request.getParameter("customerName");
    	String address=request.getParameter("customerAddress");
    	int bill=Integer.parseInt(request.getParameter("billAmount"));
    	
    	Customer customer=new Customer(id,name,address,bill);
    	CustomerDAO dao=new CustomerDAOImpl();
    	dao.InsertCustomerDetails(customer);
    	
    	
    	
    	

    	response.getWriter().println(id+"<br>");
    	response.getWriter().println(name+"<br>");
    	response.getWriter().println(address+"<br>");
    	response.getWriter().println(bill+"<br>");
    	
    	
    }

}
