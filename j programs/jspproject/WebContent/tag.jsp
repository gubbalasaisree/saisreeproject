<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="s" %>
 
<html>
<body>
 
<s:setDataSource var="ds" driver="oracle.jdbc.driver.OracleDriver"
url="jdbc:oracle:thin:@localhost:1521:orcl"
scope="session" user="scott" password="tiger" />
 
<s:query var = "cust" dataSource="${ds}">
select customer_Id,customer_Name,customer_Add,bill_amnt from customer
</s:query>
 
 
<table border=1>
<c:forEach var="row" items="${cust.rows}">
<tr>
<td><c:out value="${row.customer_Id}"/></td>
<td><c:out value="${row.customer_Name}"/></td>
<td><c:out value="${row.customer_Add}"/></td>
<td><c:out value="${row.bill_AMNT}"/></td>
</tr>
</c:forEach>
</table>