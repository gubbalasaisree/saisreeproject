package com.javatraining.customer.model;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class User {
	public static void main(String[] args) {
		ClassPathResource r=new ClassPathResource("beans2.xml");
		BeanFactory f= new XmlBeanFactory(r);
		
		Email e= (Email) f.getBean("email1");
		System.out.println(e);
		
	}

}
