package hibernatedemo2.hibernate1;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.javatraining.customer.model.Customer;



/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
      Customer customer=new Customer(103,"geeta","hyderabad",20000);
      
    	
    	//Configuration configuration=new Configuration().configure();
      
      AnnotationConfiguration config=new AnnotationConfiguration();
    	
    	SessionFactory factory=config.configure().buildSessionFactory();
    	
    	Session session= factory.openSession();
    	
    	System.out.println("data stored");
    	
    	//Query q=session.createQuery("from Customer");
    	
    	/*Criteria q=session.createCriteria(Customer.class).add(Restrictions.eq("customerId", 101))
    			.add(Restrictions.gt("billAmnt", 500));
    	
    	List<Customer> c1=q.list();
    	
    	Iterator<Customer> iterator=c1.iterator();
    	while(iterator.hasNext())
    	{
    		Customer cust=iterator.next();
    		
    		System.out.println(cust);
    	}*/
    	
    	session.save(customer);
    	
    	session.close();
    	
    	factory.close();
    	
    	
    }
}
