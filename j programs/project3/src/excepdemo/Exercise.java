package excepdemo;

public class Exercise {
	public static void main(String[] args) {
	String str="The Quick Brown Fox Jumps Over The Lazy Dog";
	String s="is";
	System.out.println("1.character at 12th index is:  "+str.charAt(12));
	System.out.println("2.does the string contains the word is:   "+str.contains(s));
	System.out.println("3."+ str+" and killed it");
	System.out.println("4.string ends with word dogs:  "+str.endsWith("dogs"));
	System.out.println("5."+str.equals("The quick brown Fox jumps over the lazy Dog"));
	System.out.println("6."+str.equals("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"));
	System.out.println("7.length of the string is: "+str.length());
	System.out.println("8."+str.compareTo("The quick brown Fox jumps over the lazy dog"));
	System.out.println("9."+str.replace("The", "A"));
	System.out.println("");
	System.out.println("12."+str.toLowerCase());
	System.out.println("13."+str.toUpperCase());
	System.out.println("14.index position of a is: "+str.indexOf("a"));
	System.out.println("15.last index position of e is: "+str.lastIndexOf("e"));
	
	}

}
