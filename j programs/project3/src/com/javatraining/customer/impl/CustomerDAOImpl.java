package com.javatraining.customer.impl;

import java.util.List;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.dbcon.DBConfig;
import com.javatraining.customer.model.Customer;

public class CustomerDAOImpl implements CustomerDAO {

	
	
	@Override
	public int InsertCustomerDetails(Customer customer) {
		Connection  connection=DBConfig.getConnection();
		int rows=0;
		try {
			PreparedStatement statement=connection.prepareStatement("insert into Customer values(?,?,?,?)");
			statement.setInt(1, customer.getCustomerId());
			statement.setString(2, customer.getCustomerName());
			statement.setString(3, customer.getCustomerAddress());
			statement.setInt(4, customer.getBillAmnt());
			rows=statement.executeUpdate();
			connection.close();
			
			}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return rows;
	}

	
	
	
	
	
	
	@Override
	public int UpdateCustomerDetails(int customerId, String newCustomerAddress, int newBillAmount) 
	{
		
		 Connection connection = DBConfig.getConnection();
	        int rows =0;
	        try 
	        {
	            PreparedStatement statement = 
	                    connection.prepareStatement("update customer set customerAddress= ? ,billAmount=? where customerId = ?");
	            statement.setString(1, newCustomerAddress);
	            statement.setInt(2, newBillAmount);
	            statement.setInt(3, customerId);
	            rows = statement.executeUpdate();
	            connection.close();
	        } 
	        catch (SQLException e) 
	        {
	            e.printStackTrace();
	        }
	        
	        return rows;
	    }
		
		
	
	
	
	
	
	
	
	

	@Override
	public int deleteCustomer(int customerId) {
		// TODO Auto-generated method stub
		Connection conn=DBConfig.getConnection();
		int rows=0;
		String query="delete * from customer where customerId=?";
		try {
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, customerId);
			rows=statement.executeUpdate();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rows;
	}
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public Customer findByCustomer_id(int customerId) {
		// TODO Auto-generated method stub
		Connection conn=DBConfig.getConnection();
		Customer customer=new Customer();
		ResultSet rs=null;
		String fquery="select * from customer where customerId=?";
		
		try {
			PreparedStatement statement=conn.prepareStatement(fquery);
			statement.setInt(1, customerId);
			rs=statement.executeQuery();
			
		
			conn.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rs;
	}

	@Override
	public boolean isCustomerAExists(int customerId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List listAllCustomers() {
		List<Customer> allcustomers=new ArrayList<Customer>();
		Connection connection=DBConfig.getConnection();
		String query="select * from customer";
		try
		{
			Statement statement= connection.createStatement();
		}
			
	
		return null;                                                                                                                                     
	}

}
