package collectiondemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.javatraining.dbcon.DBConfig;

public class Users {
	public static void main(String[] args) throws SQLException 
	{
		Scanner sc= new Scanner(System.in);
		
		Connection conn=DBConfig.getConnection();
		
		System.out.println("enter username ");
		String name=sc.next();
		System.out.println("enter password");
		int password=sc.nextInt();
		
		
		PreparedStatement pst=conn.prepareStatement("select * from users where user_name=? and password=?");
		pst.setString(1, name);
		pst.setInt(2,password);
		
		ResultSet res=pst.executeQuery();
		if(res.next())
		{
			System.out.println("valid user");
		}
		else
		{
			System.out.println("invalid user");
		}
		
		conn.close();
		
		
	}
		
	

}
