package springdemo;

import javax.annotation.Resource;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;

import com.javatraining.customer.model.Customer;

public class Client {
	public static void main(String[] args) {
		
		/*ClassPathResource resource=new ClassPathResource("beans.xml");
		BeanFactory factory=new XmlBeanFactory(resource);
		
		Customer customer=(Customer) factory.getBean("cust");
		customer.setCustomerName("sai");
		System.out.println(customer.getCustomerName());
		
		Customer c1=(Customer) factory.getBean("cust");
		System.out.println(c1);
		
		Customer c2=(Customer) factory.getBean("cust1");
		System.out.println(c2);
		
		Customer c3=(Customer) factory.getBean("cust1");
		System.out.println(c3);
		*/
		
		
		ApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
		
		
		
		
	}

}
