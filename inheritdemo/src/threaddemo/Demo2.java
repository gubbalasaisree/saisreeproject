package threaddemo;

public class Demo2 extends Thread{
	public Demo2(String x)
	{
		super(x);
		start();
	}	
			
	@Override
	public void run() {
		System.out.println("run called by : "+Thread.currentThread().getName());
	}
	public static void main(String[] args) {
		new Demo2("itpl1");
		new Demo2("itpl2");
		new Demo2("itpl3");
		System.out.println("main called :"+Thread.currentThread().getName());
	}

}
